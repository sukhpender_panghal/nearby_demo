package com.example.nearest_hospitals_demo

import android.Manifest.*
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.ArrayAdapter
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.*
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_auto_search.*

class AutoSearchActivity : AppCompatActivity(),OnMapReadyCallback {

    var mMap: GoogleMap? = null     //instance of google map
    var hisar = LatLng(29.149187,75.721657)     //for random marker set on map
    private  var placesClient: PlacesClient? = null //instanse of place client

    var adapter = MyListAdapter()    //instance of myadapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auto_search)

        Places.initialize(this,"AIzaSyBF4zlonZNw_6p23WzvlKiaQaDW0mFzLJY")   //initializing places with key
        placesClient = Places.createClient(this)
        val mapFragment
                = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
       mapFragment?.getMapAsync(this)       //linking map with mapfragment
       autotxt1.threshold=1     //from which character the search start

        autotxt1.addTextChangedListener(object : TextWatcher {      //listener for autotext response


            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                getTokenMethod()    //custom method for auto complete text view
            }
        })
       //autoCompleteFragment()       //method for replacing the fragment with google UI

    }

    private fun getTokenMethod()
    {
        val token = AutocompleteSessionToken.newInstance()

        val bounds = RectangularBounds.newInstance(
            LatLng(30.104649, 76.117873),
            LatLng(30.704649, 16.117873)
        )
        var query =autotxt1.text.toString()     //getting data from autotext
        val request =
            FindAutocompletePredictionsRequest.builder()    //predict according to word search
               // .setLocationBias(bounds)
                //.setOrigin(LatLng(30.704649, 76.717873))
                .setCountries("IN")     //country code(more than one can br provided)
                .setTypeFilter(TypeFilter.ADDRESS)
                .setSessionToken(token)
                .setQuery(query)    //the place you search in textview
                .build()
        placesClient!!.findAutocompletePredictions(request)
            ?.addOnSuccessListener { response: FindAutocompletePredictionsResponse ->
                Log.e("data ",""+Gson().toJson(response))

                var asd = response.autocompletePredictions     //incoming data response after text insertion
                val adapter1 = CustomAdapter(this,asd as ArrayList<AutocompletePrediction>)
                autotxt1.setAdapter(adapter1)
                 adapter1.notifyDataSetChanged()

            }?.addOnFailureListener { exception: Exception? ->
                if (exception is ApiException) {
                    Log.e("asd", "Place not found: " + exception.statusCode)
                }
            }
    }

   /* fun autoCompleteFragment() {
        val autocompleteFragment
                =  supportFragmentManager.findFragmentById(R.id.frag_place)
                as AutocompleteSupportFragment
        autocompleteFragment.setPlaceFields(listOf(Place.Field.LAT_LNG))

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener{
            override fun onPlaceSelected(place: Place) {

                if (mMap != null){  //map null check
                    mMap?.clear()
                }
                var getlatlong  = place.latLng
                if (getlatlong != null) {
                    hisar = getlatlong


                    val mapFragment
                            = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
                    mapFragment?.getMapAsync(this@AutoSearchActivity)
                    mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(hisar, 8.5f))
                    mMap?.addMarker(MarkerOptions().position(hisar))


                }
                Log.i("asd", "Place: ${place.name}, ${place.id}")
            }
            override fun onError(status: Status) {
                Log.i("asd1", "An error: $status")
            }
        })

    }*/
    override fun onMapReady(mMap: GoogleMap?) {

        mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(hisar,8.5f))     //camera move to provided location with provided zoom value
        mMap?.addMarker(MarkerOptions().position(hisar)
            .title("hisar")     //title on info. window of marker
            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)))    //type of marker color

        /**permission for the location*/
        if (ContextCompat.checkSelfPermission(this@AutoSearchActivity,
                permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this@AutoSearchActivity,
                    permission.ACCESS_FINE_LOCATION
                )
            ) else {
                ActivityCompat.requestPermissions(this@AutoSearchActivity,
                    arrayOf(permission.ACCESS_FINE_LOCATION), 1
                )
            }
        }

        mMap?.moveCamera(CameraUpdateFactory.newLatLng(hisar))
        mMap?.isMyLocationEnabled = true    //enabling default button of google current location on map

    }

    override fun onResume() {
        super.onResume()
        if (mMap != null){
            mMap?.clear()
        }
    }
}
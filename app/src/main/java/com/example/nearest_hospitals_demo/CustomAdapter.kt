package com.example.nearest_hospitals_demo

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import com.google.android.libraries.places.api.model.AutocompletePrediction
import kotlinx.android.synthetic.main.item_text.view.*

class CustomAdapter(val context: Context, var list: ArrayList<AutocompletePrediction>)
    : BaseAdapter(), Filterable {

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view =
            LayoutInflater.from(context).inflate(R.layout.item_text, parent, false)
        view.textview.text = list[position].getPrimaryText(null)
        return view
    }

    override fun getItem(position: Int): AutocompletePrediction? {
        return list[position]
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getFilter(): Filter? {
        return MyFilter()
    }

    override fun getCount(): Int {
        return list.size
    }

    inner class MyFilter(): Filter() {
        override fun performFiltering(p0: CharSequence?): FilterResults {
            return FilterResults()
        }
        override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
            notifyDataSetChanged()
        }
    }
}
package com.example.nearest_hospitals_demo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_auto_search.view.*

class MyListAdapter : RecyclerView.Adapter<MyListAdapter.MyViewHolder>(){

        var myData= arrayOf(String())

    override fun onCreateViewHolder(parent: ViewGroup,viewType: Int): MyViewHolder {
        val inflatedView = parent.inflate(R.layout.custom_view_autotext, false)
        return MyViewHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.itemView.autotxt1.setText(myData[5])
    }

    private fun ViewGroup.inflate(layoutRes: Int, attachToRoot: Boolean = false): View {
        return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
    }

    override fun getItemCount() = myData.size

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)
}


package com.example.nearest_hospitals_demo

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import kotlinx.android.synthetic.main.activity_main2.*
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class MainActivity2 : AppCompatActivity(), OnMapReadyCallback {

    var listpoints: ArrayList<LatLng>? = null
     var mMap: GoogleMap? = null
    var places = arrayOf(
        "hisar","haryana","hansi",
        "bhiwani","bihar","bhagana",
        "siyachin","surat","sultanpur"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        btn.setOnClickListener {
            getPlaceUrl(autotxt?.text)
        }

        /**autot complete textview*/
        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(this, android.R.layout.select_dialog_item, places)

        autotxt.setThreshold(1)
        autotxt.setAdapter(adapter)
        autotxt.setTextColor(Color.BLACK)
        //Places?.initialize(this,"AIzaSyBF4zlonZNw_6p23WzvlKiaQaDW0mFzLJY")

        /**permission for the location*/
        if (ContextCompat.checkSelfPermission(this@MainActivity2,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity2,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) else {
                ActivityCompat.requestPermissions(this@MainActivity2,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1
                )
            }
        }

        /**linking map with frgment */
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)

        listpoints = ArrayList<LatLng>()
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(mMap: GoogleMap?) {

        this. mMap =mMap
        mMap?.isMyLocationEnabled = true

        mMap?.setOnMapLongClickListener(GoogleMap.OnMapLongClickListener {
            if (listpoints?.size == 2) {
                listpoints?.clear()
                mMap.clear()
            }
            listpoints?.add(it)
            var markerOption = MarkerOptions().position(it)

            if (listpoints?.size == 1) {
                markerOption
                    .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory
                            .HUE_GREEN))
            } else {
                markerOption
                    .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory
                            .HUE_RED))
            }
            mMap.addMarker(markerOption)

            if (listpoints?.size == 2) {
                var url: String = getRequestUrl(listpoints!!.get(0), listpoints!!.get(1)) as String

                var taskRequestDirection = TaskRequestDirection()
                taskRequestDirection.execute(url)
            }
        })
    }
    /*https://maps.googleapis.com/maps/api/place/findplacefromtext/json?
    input=Museum%20of%20Contemporary%20Art%20Australia&
    inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry&
    key=YOUR_API_KEY*/
    private fun getPlaceUrl(input: Editable?): String {

        val input = "input=" + autotxt.text
        val inputtype = "inputtype=textquery"
        val output = "json"
        val api_key="AIzaSyBF4zlonZNw_6p23WzvlKiaQaDW0mFzLJY"
        val url1 = "https://maps.googleapis.com/maps/api/place/findplacefromtext/" + output +
                "?" + input +
                "&" + inputtype +
                "&key=" + api_key
        return url1
    }


    private fun getRequestUrl(origin: LatLng, dest: LatLng): String {

        val str_org = "origin=" + origin.latitude + "," + origin.longitude
        val str_destination = "destination=" + dest.latitude + "," + dest.longitude
        val output = "json"
        val api_key="AIzaSyBF4zlonZNw_6p23WzvlKiaQaDW0mFzLJY"
        val url = "https://maps.googleapis.com/maps/api/directions/"+ output +
                "?" + str_org +
                "&" + str_destination +
                "&key=" + api_key
        return url
         }

 inner class TaskRequestDirection : AsyncTask<String, Void, String>() {

        override fun doInBackground(vararg p0: String): String {

            var responseString  = requestDirection(p0[0])
            return responseString
        }

        private fun requestDirection(reqUrl: String): String {
            var responseString = ""
            var inputStream: InputStream? = null
            var httpConnection: HttpURLConnection? = null

            try {
                val url = URL(reqUrl)
                httpConnection = url.openConnection() as HttpURLConnection?
                httpConnection?.connect()

                inputStream = httpConnection?.inputStream
                val inputStreamReader = InputStreamReader(inputStream)
                val bufferReader = BufferedReader(inputStreamReader)

                val stringBuffer = StringBuffer()
                var line :String?= ""
                line = bufferReader.readLine()
                while (line != null) {
                    stringBuffer.append(line)
                    line = bufferReader.readLine()
                }

                var responseString1 = stringBuffer.toString()
                bufferReader.close()
                inputStreamReader.close()
                return responseString1

            } catch (e: Exception) {
                e.printStackTrace()
            }
            finally {
                if (inputStream != null) {
                    inputStream.close()
                }
                httpConnection?.disconnect()
            }
            return responseString
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            val taskParser = TaskParser()
            taskParser.execute(result)
        }

    }

 inner class TaskParser : AsyncTask<String, Void, List<List<HashMap<String, String>>>>() {

        override fun doInBackground(vararg p0: String?): List<List<HashMap<String, String>>>? {

            var jsonObject: JSONObject
            var routes: List<List<HashMap<String, String>>>?
            try {
                jsonObject = JSONObject(p0[0])
                val dataPareser = DataParser()
                routes = dataPareser.parse(jsonObject)
                return routes
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                return null
            }
        }

        override fun onPostExecute(result: List<List<HashMap<String, String>>>?) {
            super.onPostExecute(result)

            var points: ArrayList<LatLng>
            var lineOptions: PolylineOptions? = null

            if (result != null) {
                for (i in 0 until result.size) {
                    points = ArrayList()
                    lineOptions = PolylineOptions()
                    val path: List<HashMap<String, String>> = result[i]
                    for (j in 0 until path.size) {
                        val point: HashMap<String, String> = path[j]
                        val lat: Double = point.get("lat")!!.toDouble()
                        val lng: Double = point.get("lng")!!.toDouble()
                        val position = LatLng(lat, lng)
                        points.add(position)
                    }
                    lineOptions.addAll(points)
                    lineOptions.width(8f)
                    lineOptions.color(Color.GREEN)
                    lineOptions.geodesic(true)
                }
                if (lineOptions != null){
                    mMap?.addPolyline(lineOptions)
                }
                else{
                    Log.d("errord","direction not found")
                }
            }
        }
    }
}
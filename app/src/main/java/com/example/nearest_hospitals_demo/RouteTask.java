/*

package com.example.nearest_hospitals_demo;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

*/
/**
 * Created by viren on 7/12/17.
 *//*


public class RouteTask extends AsyncTask<String, Void, String> {

    Context context;
    RouteInterface listner;
    private String TAG = "RouteTask";
    private JSONObject distance, duration;

    public RouteTask(Context context, RouteInterface listner) {
        this.context = context;
        this.listner = listner;
    }

    @Override
    protected String doInBackground(String... place) {
        // For storing data from web service
        String data = "";
        Log.d(TAG, "Route task doInBackground");
        // Obtain browser key from https://code.google.com/apis/console
        String key = "&key=" + URL.GOOGLEAPIKEY.trim();


String sourceID = "";
        if (target != null) {


            sourceID = target.latitude + "," + target.longitude;
        }

        String destinationID = "";
        if (destPlace.getLatLng() != null) {
            destinationID = destPlace.getLatLng().latitude + "," + destPlace.getLatLng().longitude;
        }

        StringBuilder wayPoints = new StringBuilder();
        wayPoints.append("?origin=" + place[0] + "&destination=" + place[1]);
        if (place.length > 2) {
            if (place[2].length() > 1) {
                if (place.length > 3) {
                    wayPoints.append("&waypoints=via:" + place[3] + "|" + place[2]);
                } else
                    wayPoints.append("&waypoints=via:" + place[2]);
            }

        }

        // + place[0] + "&destination=" + place[1] +
        //                "&waypoints=via:"+place[2]

        if (!place[0].isEmpty() && !place[1].isEmpty()) {

            String url = "https://maps.googleapis.com/maps/api/directions/json" + wayPoints
                    + "&optimize:true&key=" + URL.GOOGLEAPIKEY.trim() + "&sensor=false&units=metric&mode=driving&provideRouteAlternatives=true";

            try {
                // Fetching the data from web service in background
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d(TAG, e.toString());
            }
            Log.d(TAG, url + " \n" + data);
        }
        return data;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        try {
            ParserTask parserTask = new ParserTask(listner);
            parserTask.execute(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String downloadUrl(String strUrl) {

        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            java.net.URL url = new java.net.URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            //e.printStackTrace();
        } finally {
            try {
                iStream.close();
            } catch (IOException e) {
                // e.printStackTrace();
            }
            urlConnection.disconnect();
        }
        return data;

    }


*/
/**
     * A class to parse the Google Places in JSON format
     *//*


    public class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        RouteInterface listner;

        public ParserTask(RouteInterface listner) {
            this.listner = listner;
        }

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                //Log.d("ParserTask", jsonData[0].toString());

                //Log.d("ParserTask", parser.toString());
                JSONObject routes1 = jObject.getJSONArray("routes").getJSONObject(0);
                JSONObject legs = routes1.getJSONArray("legs").getJSONObject(0);
                distance = legs.getJSONObject("distance");
                duration = legs.getJSONObject("duration");
                Log.d(TAG, "distance:" + distance.getString("text") + "\n time" + duration.getString("text"));
                DataParser parser = new DataParser();
                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            ArrayList<ArrayList<LatLng>> routes = new ArrayList<>();

            // Traversing through all the routes
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    ArrayList points = new ArrayList<>();


                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    routes.add(points);

                }
            }

            this.listner.onRouteLoad(routes, distance, duration);
        }
    }

}
*/

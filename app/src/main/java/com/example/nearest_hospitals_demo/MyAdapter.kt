package com.example.nearest_hospitals_demo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.custom_hospital_view.view.*

class MyAdapter : RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

    var myDataset: List<responseClass.Result> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup,viewType: Int): MyViewHolder {
        val inflatedView = parent.inflate(R.layout.custom_hospital_view, false)
        return MyViewHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.txt_name.text = myDataset[position].name
        holder.itemView.place_id.text = myDataset[position].rating.toString()
        holder.itemView.type1.text = myDataset[position].vicinity

    }
   private fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
        return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
    }
    override fun getItemCount() = myDataset.size

    class MyViewHolder(view:View) : RecyclerView.ViewHolder(view)
}


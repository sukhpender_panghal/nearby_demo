package com.example.nearest_hospitals_demo

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    var longitude: Double? = null
    var latitude: Double? = null
    var apiInterface: ApiInterface? = null
    var viewAdapter = MyAdapter()
    val handler = Handler()

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (ContextCompat.checkSelfPermission(
                this@MainActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this@MainActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) else {
                ActivityCompat.requestPermissions(
                    this@MainActivity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1
                )
            }
        }
        handler.postDelayed({
        }, 2000)

        hospitals.setOnClickListener {
            getHospitalList()
        }
        restauant.setOnClickListener {
            getRestaurantsList()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getHospitalList() {

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
            latitude = location?.latitude
            longitude = location?.longitude

            recyclerView.layoutManager = LinearLayoutManager(this)
            recyclerView.adapter = viewAdapter
            apiInterface = APIClient().getClient()?.create(ApiInterface::class.java)

            val call: Call<responseClass> = apiInterface!!.getHospitals(
                "" + location?.latitude + "," + location?.longitude,
                1500,
                "hospital",
                "hospitals",
                "AIzaSyBF4zlonZNw_6p23WzvlKiaQaDW0mFzLJY"
            )
            call.enqueue(object : Callback<responseClass> {
                override fun onFailure(call: Call<responseClass?>, t: Throwable) {
                }

                override fun onResponse(
                    call: Call<responseClass?>,
                    response: Response<responseClass?>
                ) {
                    if (response != null) {
                        viewAdapter.myDataset = response.body()!!.results
                        viewAdapter.notifyDataSetChanged()
                        Log.d("success", "" + response)
                    } else {
                        Log.d("error", "failed")
                    }
                }
            })
        }
    }

    @SuppressLint("MissingPermission")
    private fun getRestaurantsList() {

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                latitude = location?.latitude
                longitude = location?.longitude

                recyclerView.layoutManager = LinearLayoutManager(this)
                recyclerView.adapter = viewAdapter

                apiInterface = APIClient().getClient()?.create(ApiInterface::class.java)
                val call: Call<responseClass> = apiInterface!!.getHospitals(
                    "" + latitude + "," + longitude,
                    1500,
                    "restaurant",
                    "restaurants",
                    "AIzaSyBF4zlonZNw_6p23WzvlKiaQaDW0mFzLJY"
                )
                call.enqueue(object : Callback<responseClass> {
                    override fun onFailure(call: Call<responseClass?>, t: Throwable) {
                    }

                    override fun onResponse(
                        call: Call<responseClass?>,
                        response: Response<responseClass?>
                    ) {
                        if (response != null) {
                            viewAdapter.myDataset = response.body()!!.results
                            viewAdapter.notifyDataSetChanged()
                            Log.d("success", "" + response)
                            var a = response.body()
                            Log.d("name", "" + a)
                        } else {
                            Log.d("error", "failed")
                        }
                    }
                })
            }
    }
}
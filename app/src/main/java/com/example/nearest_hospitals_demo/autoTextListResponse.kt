package com.example.nearest_hospitals_demo


import com.google.gson.annotations.SerializedName

data class autoTextListResponse(
    @SerializedName("place_id")
    val place_id: String,
    @SerializedName("getFullText")
    val getFullText: String)
{

}
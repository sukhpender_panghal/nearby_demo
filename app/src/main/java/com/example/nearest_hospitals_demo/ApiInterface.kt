package com.example.nearest_hospitals_demo

import com.google.android.gms.maps.model.LatLng
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
   /* @GET("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=29.149187,75.721657&radius=2000&type=hospitals&keyword=hospitals&key=AIzaSyBF4zlonZNw_6p23WzvlKiaQaDW0mFzLJY")*/

    @GET("https://maps.googleapis.com/maps/api/place/nearbysearch/json")
    fun getHospitals(
        @Query("location") location: String?,
        @Query("radius") radius:Int,
        @Query("type") type:String,
        @Query("keywords") keywords:String,
        @Query("key") key:String):retrofit2.Call<responseClass>

    /*https://maps.googleapis.com/maps/api/directions/json?origin=Hisar&destination=Hansi&key=AIzaSyBF4zlonZNw_6p23WzvlKiaQaDW0mFzLJY*/

    @GET("https://maps.googleapis.com/maps/api/directions/json")
    fun getRoute(
        @Query("origin") origin: String?,
        @Query("destination") destination:String,
        @Query("key") key:String):retrofit2.Call<DirectionApiResponse>
}